import 'package:badges/src/badge.dart' as BadgesPackage;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/cart_model.dart';
import 'package:shop/provider.dart';
import 'package:shop/screen.dart';
import 'package:shop/db.dart';

class ProductListScreen extends StatelessWidget {
  final List<String> productName = [
    'Raccoon',
    'Girl Plush, Anime',
    'Deer',
    'Rabbit',
    'Bear',
    'Cat',
    'Dog',
    'Turtle',
    'Shark',
    'Unicorn',
  ];

  final List<String> productUnit = List.filled(10, 'Rates');
  final List<int> productPrice = [200, 200, 250, 450, 1100, 102, 605, 720, 150, 205];

  final List<String> productImage = [
    'https://www.pngkey.com/png/full/888-8884956_cute-bellzi-raccoon-stuffed-animal-plush-toy-cute.png?fbclid=IwAR3CeLG6n_cCJK-IXXFUOUNWb9C8DcBWlh_jVt33FBSz797lU5dDWU2HEos',
    'https://www.pngmart.com/files/13/Plush-PNG-HD.png',
    'https://static.vecteezy.com/system/resources/previews/011/662/700/non_2x/cute-stuffed-animal-free-png.png?fbclid=IwAR1H3gMmz-P_7Vs1J7NcifxXJ8uH63Gmzad4Juk0EhZ7dFoh6Fv3UYVDm1c',
    'https://www.pngkey.com/png/full/549-5496172_chantilly-the-large-deluxe-bunny-stuffed-toy.png?fbclid=IwAR2QGAY_Uvt9EoXeMx_vXLIEVQp_X482awfaNiEiK0jjq2PRisjmZVPmaiY',
    'https://i.pinimg.com/originals/f6/f5/b2/f6f5b23c199a953daff787dc9cbeaa5f.png?fbclid=IwAR01htu88lLAXRcz6zBNsr70y0a4-cZcfkW6Y3uXExE2mB3IzCDazMs6Sg4',
    'https://static.vecteezy.com/system/resources/previews/024/724/518/non_2x/stuffed-cat-toy-isolated-on-transparent-background-fluffy-soft-cute-kitten-toy-generative-ai-png.png?fbclid=IwAR3tzBhecGODs_Q3Rv8yZOJExo1gxQiqX9AJnqWyJPhRBgovcCxsfno3eYI',
    'https://static.vecteezy.com/system/resources/previews/009/858/783/original/dog-doll-isolated-on-white-background-with-clipping-path-png.png?fbclid=IwAR0q8ebPkFMdlkH3dtFw9cQd4JRTtuXRZx1w_14MHAJcz02zVmzm0lo4R2M',
    'https://www.happytails.com/wp-content/uploads/2020/08/00503_TURTLE_.png?fbclid=IwAR1y51TgtwcUcWX9y1SXTsGPig1hifvKwgZB28sv8zAIIeYdFrIN0SvUEqE',
    'https://static.vecteezy.com/system/resources/previews/024/724/500/original/stuffed-shark-toy-isolated-on-transparent-background-fluffy-soft-shark-fish-toy-generative-ai-png.png?fbclid=IwAR3sxR48fxP8EnGgHpGtqzWwBHRpDVHMCXgfxVbHaRxEAYu7CxLc4bZZ6ns',
    'https://static.vecteezy.com/system/resources/previews/024/724/497/original/stuffed-unicorn-toy-isolated-on-transparent-background-fluffy-soft-cute-unicorn-toy-generative-ai-png.png?fbclid=IwAR09s3Wy_iY27kS4WYFZ84sRv8lcU89QzViXRJ6aBBa2o5qAzNU4kihYeHA',
  ];

  DBHelper? dbHelper = DBHelper();

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Product List'),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen()));
            },
            child: Center(
              child: BadgesPackage.Badge(
                showBadge: true,
                badgeContent: Consumer<CartProvider>(
                  builder: (context, value, child) {
                    return Text(value.getCounter().toString(), style: TextStyle(color: Colors.white));
                  },
                ),
                animationDuration: Duration(milliseconds: 300),
                child: Icon(Icons.shopping_cart_sharp),
              ),
            ),
          ),
          SizedBox(width: 20.0)
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.red),
              accountName: Text(
                "ToyShop",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ),
              ),
              accountEmail: Text(
                "MiyShinyShop@gmail.com",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/images/shopstore.png'),
                radius: 60,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.shopping_bag_rounded,
              ),
              title: const Text('Shop'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            AboutListTile(
              icon: Icon(
                Icons.account_circle,
              ),
              child: Text('ABOUT'),
              applicationIcon: Icon(
                Icons.account_circle,
              ),
              applicationName: 'About',
              aboutBoxChildren: [
                Image.asset("assets/images/sor.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("IDTM",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("รหัส : 6450110012",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("ชื่อ : วีรภัทร ส่งศรีจันทร์",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                SizedBox(height: 20,),
                Image.asset("assets/images/pro.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("IDTM",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("รหัส : 6450110001",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("ชื่อ : กฤษกร ธนาลัทธกุล",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
              ],
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: productName.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Image(
                              height: 100,
                              width: 100,
                              image: NetworkImage(productImage[index]),
                            ),
                            SizedBox(width: 10,),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    productName[index],
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 5,),
                                  Text(
                                    '${productUnit[index]} \$${productPrice[index]}',
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 5,),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: InkWell(
                                      onTap: () {
                                        dbHelper!.insert(
                                          Cart(
                                            id: index,
                                            productId: index.toString(),
                                            productName: productName[index],
                                            initialPrice: productPrice[index],
                                            productPrice: productPrice[index],
                                            quantity: 1,
                                            unitTag: productUnit[index],
                                            image: productImage[index],
                                          ),
                                        ).then((value) {
                                          cart.addTotalPrice(productPrice[index].toDouble());
                                          cart.addCounter();

                                          final snackBar = SnackBar(
                                            backgroundColor: Colors.green,
                                            content: Text('Product is added to cart'),
                                            duration: Duration(seconds: 1),
                                          );

                                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                        }).onError((error, stackTrace) {
                                          final snackBar = SnackBar(
                                            backgroundColor: Colors.red,
                                            content: Text('Product is already added in cart'),
                                            duration: Duration(seconds: 1),
                                          );

                                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                        });
                                      },
                                      child: Container(
                                        height: 35,
                                        width: 100,
                                        decoration: BoxDecoration(
                                          color: Colors.pink,
                                          borderRadius: BorderRadius.circular(12),
                                        ),
                                        child: const Center(
                                          child: Text(
                                            'Add to cart',
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
